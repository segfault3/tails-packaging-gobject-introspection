-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: gobject-introspection
Binary: libgirepository-1.0-1, libgirepository1.0-dev, libgirepository1.0-doc, gobject-introspection, gir1.2-glib-2.0, gir1.2-freedesktop
Architecture: any all
Version: 1.50.0-1
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Emilio Pozuelo Monfort <pochu@debian.org>, Iain Lane <laney@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: https://wiki.gnome.org/GObjectIntrospection
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gobject-introspection/
Vcs-Svn: svn://anonscm.debian.org/svn/pkg-gnome/desktop/unstable/gobject-introspection
Testsuite: autopkgtest
Testsuite-Triggers: build-essential, file, gir1.2-gtk-3.0, libcairo2-dev
Build-Depends: debhelper (>= 10), gnome-pkg-tools (>= 0.10), python3-dev, pkg-config, flex, gtk-doc-tools (>= 1.19), bison, libglib2.0-dev (>= 2.50.0), libcairo2-dev, libffi-dev (>= 3.0.0), libtool, python3-mako
Build-Depends-Indep: libglib2.0-doc
Package-List:
 gir1.2-freedesktop deb introspection optional arch=any
 gir1.2-glib-2.0 deb introspection optional arch=any
 gobject-introspection deb devel optional arch=any
 libgirepository-1.0-1 deb libs optional arch=any
 libgirepository1.0-dev deb libdevel optional arch=any
 libgirepository1.0-doc deb doc optional arch=all
Checksums-Sha1:
 5907f70bd275dceb4d3cb825ddce82cb1fdb4242 1415700 gobject-introspection_1.50.0.orig.tar.xz
 fa138e8bd199306b19ed9839c537bd7992659d1b 19116 gobject-introspection_1.50.0-1.debian.tar.xz
Checksums-Sha256:
 1c6597c666f543c70ef3d7c893ab052968afae620efdc080c36657f4226337c5 1415700 gobject-introspection_1.50.0.orig.tar.xz
 94c0a9a7be53d1a8d35c449193faf50c9f9796319d40dc42f9d65f3b677a6999 19116 gobject-introspection_1.50.0-1.debian.tar.xz
Files:
 5af8d724f25d0c9cfbe6df41b77e5dc0 1415700 gobject-introspection_1.50.0.orig.tar.xz
 1199e6144cb7905083a28e7683f4869e 19116 gobject-introspection_1.50.0-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIuBAEBCAAYBQJX4s1wERxiaWVibEBkZWJpYW4ub3JnAAoJEGrh3w1gjyLctgQP
/2PP1T9JW9LP/8+4O/QTa3jm12vZ2252KDMk93OpaInJgf/f7M1h6klCowX94Fq/
N8l3TZZvEFK8IyPxIaHs19Wlwy4uHPADKjjf9OCBFGsizOgsdEeVZM4RjrbvZToF
tvbv410uwtmbir5xRp1gFOjqMgMKQcc49rPFTV0GnxogYhK3QZRp9UPUAYS3VF2z
8FH5w+GoJqrBQXYbQC+uwGoT0dzvZ4pUN/KSTWy84umzGR6w/7O3cBsNFNBDI63r
CRBCMKNHvoOFwHSj2KZDBRYlCtrxBpUwp3zf1m+F9swc39gdj0JTdaY0ubonNfHH
U24OXaMPbptgzV9YE8mCZ2OftBpVu8WshIgGPWcQzhC0CVvUrf5Comov+YwuOfQI
2+SdsN0+QetsCxOB2jdABAFTT0tfAJGe42vocCDkyZJPDqXwZX/jiH5j2dGXhpLO
+QnMhPmnnouZlT5faXMoVjBo1TS26SK54Z3+VfAYXTMC2sVUJVU7/0919fZAtDKz
iWFtdWIdcuVheVaXoLK67ezvLNn0liGD8TSxzwXftt4Rff8YjYXzeSMKhvOCcTjC
5RTaW8BnNicSN4Zlv5BqpWPmbZvYAk4logAdj/fXW2nvYMv4FGEJLyq3L01DKKYw
bOd1MtGlgVWFb0UlMHuoaPyW4omc3B6Ii4hjxkB9JuPU
=6Wj6
-----END PGP SIGNATURE-----
